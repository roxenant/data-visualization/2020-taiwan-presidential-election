# !/bin/bash

echo '[INFO] This script is used to run the main python code.'
echo '[INFO] Activate the virtual environment.'
source tpe/bin/activate

echo '[INFO] Data preprocessing...'
python3 src/data_preparation.py

echo '[INFO] open jupyter notebook.'
jupyter notebook

echo '[INFO] Deactivate the virtual environment.'
deactivate

echo '[INFO] End.'