# -*- coding: utf-8 -*-
# data_preparation.py
import os
import json
import xlrd
from math import floor

# Load xls file
path = "data/總統-各投票所得票明細及概況(Excel檔)"
twGoeJsonFilePath = "data/tw_town_geo.json"
documentList = []
twGoeJsonDict = {}
outputFile = "data/tw_vote_geo.json"

with open(twGoeJsonFilePath, 'r') as f:
	twGoeJsonDict = json.load(f)


for documentName in os.listdir(path):
	if documentName.startswith("總統-A05-2-"):
		documentList.append(xlrd.open_workbook(path + "/" + documentName))

townDict = {}
for document in documentList:
	table = document.sheets()[0]
	for i in range(6, table.nrows):
		row = table.row_values(i)[:4]
		row[0] = row[0][1:-1]
		townDict[row[0]] = row[1:4]

for feature in twGoeJsonDict["features"]:
	try: 
		feature["properties"]["宋楚瑜/余湘"] = int(townDict[feature["properties"]["TOWNNAME"][:-1]][0].replace(',', ''))
		feature["properties"]["韓國瑜/張善政"] = int(townDict[feature["properties"]["TOWNNAME"][:-1]][1].replace(',', ''))
		feature["properties"]["蔡英文/賴清德"] = int(townDict[feature["properties"]["TOWNNAME"][:-1]][2].replace(',', ''))
		total = feature["properties"]["宋楚瑜/余湘"] + feature["properties"]["韓國瑜/張善政"] + feature["properties"]["蔡英文/賴清德"]
		if not total:
			feature["properties"]["支持率(宋楚瑜/余湘)"] = feature["properties"]["支持率(韓國瑜/張善政)"] = feature["properties"]["支持率(蔡英文/賴清德)"] = 0
			continue
		# import pudb;pu.db
		feature["properties"]["V支持率%(宋楚瑜/余湘)"] = round(feature["properties"]["宋楚瑜/余湘"] / total, 2)
		feature["properties"]["V支持率%(韓國瑜/張善政)"] = round(feature["properties"]["韓國瑜/張善政"] / total, 2)
		feature["properties"]["V支持率%(蔡英文/賴清德)"] = round(feature["properties"]["蔡英文/賴清德"] / total, 2)
		feature["properties"]["支持率%(宋楚瑜/余湘)"] = str(round(feature["properties"]["宋楚瑜/余湘"] / total, 2))
		feature["properties"]["支持率%(韓國瑜/張善政)"] = str(round(feature["properties"]["韓國瑜/張善政"] / total, 2))
		feature["properties"]["支持率%(蔡英文/賴清德)"] = str(round(feature["properties"]["蔡英文/賴清德"] / total, 2))
		maxVotes = max(feature["properties"]["宋楚瑜/余湘"], feature["properties"]["韓國瑜/張善政"], feature["properties"]["蔡英文/賴清德"])
		parties = []
		if maxVotes == feature["properties"]["宋楚瑜/余湘"]:
			parties.append('親民黨')
		if maxVotes == feature["properties"]["韓國瑜/張善政"]:
			parties.append('國民黨')
		if maxVotes == feature["properties"]["蔡英文/賴清德"]:
			parties.append('民進黨')
		feature["properties"]['政黨偏好'] = '/'.join(parties)

	except KeyError:
		feature["properties"]["宋楚瑜/余湘"] = '*'
		feature["properties"]["韓國瑜/張善政"] = '*'
		feature["properties"]["蔡英文/賴清德"] = '*'
		feature["properties"]["支持率%(宋楚瑜/余湘)"] = '*'
		feature["properties"]["支持率%(韓國瑜/張善政)"] = '*'
		feature["properties"]["支持率%(蔡英文/賴清德)"] = '*'
		feature["properties"]['政黨'] = '*'
		print("Missing town: " + feature["properties"]["TOWNNAME"] + ' Replace by \'*\'')

with open(outputFile, "w") as f:
	json.dump(twGoeJsonDict, f)


	