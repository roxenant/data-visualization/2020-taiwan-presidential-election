# 2020-taiwan-presidential-election

## INTRODUCTION
Visualize the result of Taiwan president election in 2020.<br>
In the layer 1 to layer 3, we visualize the percentage of supporting rate for each president candidate in every town.<br>
In layer 4, we visualize which party gained the majority of support depends on the color of the party.<br>
Please visit [here](https://roxenant.gitlab.io/data-visualization/2020-taiwan-presidential-election/) to interact with the fun stufff :D  

## RESULTS
### Layer 1: Support rate of president candidate Ing-Wen Tsai / vice president candidate Ching-te Lai
![](result/screenshot/Layer1.png)
### Layer 2: Support rate of president candidate Kuo-yu Han / vice president candidate San-cheng Chang
![](result/screenshot/Layer2.png)
### Layer 3: Support rate of president candidate Chu-yu Soong / vice president candidate Sandra Yu
![](result/screenshot/Layer3.png)
### Layer 4: Majority of party perference
![](result/screenshot/Layer4.png)


## LINKS:

1. Keplergl user guide: [kepler.gl](https://docs.kepler.gl/docs/keplergl-jupyter) 
2. Voting data: [台灣中選會網站](https://db.cec.gov.tw/histMain.jsp?voteSel=20200101A1) 
3. Taiwan geospatial data: [g0v](https://github.com/g0v/twgeojson/tree/master/json)
4. Swedish open data: [öppna data och PSI](https://oppnadata.se/#noscroll)
5. Taiwanese open data: [政府資料開放平臺](https://data.gov.tw/)

## LOG 2020/06/06

### History
1. 比較兩種tools: keplergl(better) and folium
2. 決定視覺化主題: 2020 台灣選舉開票結果
3. 整理project template
4. 寫腳本
5. 找geojson資料: town level and vill level
6. 視覺化town邊界 and vill 邊界(考慮執行速度跟視覺話效果決定用town level)

### To-Do
1. Map 開票資料 跟 geo 資料
2. Group 開票資料

## LOG 2020/06/13

### History
1. 整理資料，在GeoJson檔加入各個候選人票數(town level)
2. 在腳本中加入安裝xlrd library
3. 改excel檔案內容: 臺->台

### To-Do
1. 更改config使hover內容顯示三個候選人得票數
2. Layer 1 to 3: 三組候選人資料視覺化
3. Layer 4: 政黨顏色視覺化
4. Deploy 到 heroku
5. CICD設定

## LOG 2020/06/20

### History
1. 更改 config 使 hover 內容顯示三個候選人得票數
2. Layer 1 to 3: 三組候選人資料視覺化
3. Layer 4: 政黨顏色視覺化
4. 編輯 Readme
5. CI/CD設定