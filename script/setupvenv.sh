# !/bin/bash
echo '[INFO] This script is used to set up a python3 virtual environment for running this project.'

echo '[INFO] Create and activate a new venv tpe.'
python3 -m venv ./tpe
source ./tpe/bin/activate

echo '[INFO] Install the needed python packages.'
# python3 -m pip install -r setup_environment/requirements.txt
python3 -m pip install jupyter notebook
python3 -m pip install keplergl
python3 -m pip install ipywidgets
python3 -m pip install geopandas
python3 -m pip install xlrd
python3 -m pip freeze > setup_environment/requirements.txt

echo '[INFO] Deactivate the virtual environment.'
deactivate

echo '[INFO] Finished setting up the python3 virtual environment for running this project.'

